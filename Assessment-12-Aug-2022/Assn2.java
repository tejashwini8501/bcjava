package Assignment2;

import java.util.Scanner;

class VoterID
{
	private String name;
	private int age;
	public void VoterID(String n, int a)
	{
		name = n;
		age = a;
	}
	public void disp(int a)
	{
		age = a;
		if(a>=18)
		{
			System.out.println("Valid voter");
		}
		else
		{
			System.out.print("Invalid voter");
		}
	}
}
public class Assn2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		VoterID obj = new VoterID();
		System.out.println("Enter name: ");
		String n = sc.next();
		System.out.println("Enter age: ");
		int a = sc.nextInt();
		obj.VoterID(n, a);
		obj.disp(a);
	}
	
}
