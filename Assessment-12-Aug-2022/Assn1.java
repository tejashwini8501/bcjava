package Assignment1;

import java.util.Scanner;

class StuDetails
{
	private int usn;
	private String name;
	private long contact;
	private String email;
	public void StuDetails(int a, String b, long c, String d)
	{
		usn = a;
		name = b;
		contact = c;
		email = d;
	}
	public void display()
	{
		System.out.println("Student details are:");
		System.out.println("USN: "+usn);
		System.out.println("Name: "+name);
		System.out.println("Contact No: "+contact);
		System.out.println("Email ID: "+email);
	}
}
public class Assn1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter usn: ");
		int a = sc.nextInt();
		System.out.println("Enter name: ");
		String b = sc.next();
		System.out.println("Enter contact: ");
		long c = sc.nextLong();
		System.out.println("Enter email: ");
		String d = sc.next();
		StuDetails obj = new StuDetails();
		obj.StuDetails(a, b, c, d);
		obj.display();
		
	}

}
