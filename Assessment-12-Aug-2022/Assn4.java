package Assignment4;

class SingleTn
{
	static SingleTn obj = null;
	
	private SingleTn()
	{
		
	}
	public static SingleTn getObj()
	{
		if(obj == null)
		{
			obj = new SingleTn();
		}
		return obj;
	}
}
public class Assn4 {
	public static void main(String[] args) {
		SingleTn obj = SingleTn.getObj();
		System.out.println("Hashcode of object created using single ton method "+obj.hashCode());
		SingleTn obj1 = SingleTn.getObj();
		System.out.println("Hashcode of object created normally "+obj.hashCode());
	}
}