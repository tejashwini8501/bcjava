package p1;

import java.util.Scanner;

public class DataStd {
	
	public int id;
	public int usn;
	public String name;
	public String company;
	public int score;
	
	public DataStd()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter id");
		id = sc.nextInt();
		System.out.println("Enter usn");
		usn = sc.nextInt();
		System.out.println("Enter score");
		score = sc.nextInt();
		System.out.println("Enter name");
		name = sc.next();
		System.out.println("Enter company");
		company = sc.next();
	}
	
	public void get()
	{
		System.out.println("name\t"+ name);
		System.out.println("id\t"+ id);
		System.out.println("usn\t"+ usn);
		System.out.println("company\t"+ company);
		System.out.println("score\t"+ score);
	}
}

