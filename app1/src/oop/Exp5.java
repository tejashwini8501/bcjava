package oop;

class SingleTn
{
	static SingleTn obj = null;
	
	private SingleTn()
	{
		
	}
	public static SingleTn getObj()
	{
		if(obj == null)
		{
			obj = new SingleTn();
		}
		return obj;
	}
}
public class Exp5 {
	public static void main(String[] args) {
		SingleTn obj1 = SingleTn.getObj();
	}
}
