package oop;


class Employee
{
	//default constructor
	public Employee()
	{
		System.out.println("default");
	}
	
	//parameterised constructor
	public Employee(int a, String name)
	{
		System.out.println("parameterised");
	}
	
	//overloaded constructor
	public Employee(double a, char name)
	{
		System.out.println("overloaded");
	}
	
	//object parameterised constructor
	public Employee(Employee obj)
	{
		System.out.println("object para");
	}
}

public class Exp3 
{
	public static void main(String[] args)
	{
		Employee e1 = new Employee();
		Employee e2 = new Employee(10, "teju");
		Employee e3 = new Employee(10.2, 'f');
		Employee e4 = new Employee(e1);
	}
	
}
