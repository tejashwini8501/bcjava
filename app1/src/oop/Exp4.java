package oop;

class Student
{
	public Student()
	{
		this(20);//this should always be used as first statement in constructor block
		System.out.println("default constructor");//5
	}
	
	public Student(int a)
	{
		this("teju");
		System.out.println("single args");
	}
	public Student(String n)
	{
		System.out.println("string args");
	}
	static
	{
		System.out.println("static block");//3
	}
	{
		System.out.println("instance block");//4
	}
	
	public Student getObj()//factory method
	{
		return new Student();
	}
}

public class Exp4 {
	static
	{
		System.out.println("static block");//1
	}
	public static void main(String[] args)
	{
		System.out.println("main method");//2
		
		Student obj = new Student();
		
		obj.getObj();
	}
}
//static block runs only once
//instance block runs before constructor block