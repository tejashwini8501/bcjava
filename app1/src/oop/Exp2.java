package oop;

class MethodChain 
{
	public void method1()
	{
		System.out.println("method 1");
		method2();
	}
	public void method2()
	{
		System.out.println("method 2");
		methodn();
	}
	public void methodn()
	{
		System.out.println("method n");
	}
}

public class Exp2
{
	public static void main(String[] args)
	{
		MethodChain obj = new MethodChain();
		obj.method1();
	}
}
