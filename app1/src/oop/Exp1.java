package oop;

public class Exp1 {
	
	int id = 50;
	static int pin = 560076;
	
	public void disp()
	{
		System.out.println(id+" "+Exp1.pin);	
	}
	//factory method
	public Exp1 getObj(Exp1 obj)
	{
		return obj;
	}
	public Exp1 getObj1()
	{
		return new Exp1();
	}
	public Exp1 getObj2()
	{
		Exp1 obj2 = new Exp1();
		return obj2;
	}
	
	public static void main(String[] args)
	{
		Exp1 obj = new Exp1();
		obj.disp();
		
		System.out.println(obj.hashCode());
	}

}
